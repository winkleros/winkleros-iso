#+TITLE: winkleros-iso
#+DESCRIPTION: A customized "archiso" for building the DTOS ISO.
#+AUTHOR: Derek Taylor (DistroTube)

* About winkleros-iso
This repository is a customized "archiso" for building the DTOS ISO.  Want a DTOS ISO?  Here is everything you need to build it yourself.  Don't want to build the ISO yourself?  I don't blame you!  Here is a link to [[https://sourceforge.net/projects/winkleros/][DTOS on sourceforge]].  You can download a pre-built ISO from there. 

* Dependencies
To make the DTOS ISO, you need to have either 'archiso' installed from the standard Arch repos or 'archiso-git' installed from the AUR.  Some Arch-based distros do not have the 'archiso' program in their repos, so if you are building the DTOS ISO on such a distro, you would either need to install 'archiso-git' from the AUR or spin up an Arch virtual machine and build the ISO within a VM.

* Instructions to build the ISO
Clone this repo and then cd into the winkleros-XXXX directory and run:

#+begin_example
sudo mkarchiso -v -w output/ -o output/ releng/
#+end_example

This will create an =output= directory in the winkleros-XXXX directory.  It will use this directory as a working directory to build the ISO.

* The DTOS live user

| NAME | winkleros |
| PASS | winkleros |
